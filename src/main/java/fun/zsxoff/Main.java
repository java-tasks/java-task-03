package fun.zsxoff;


import fun.zsxoff.bank.Client;
import fun.zsxoff.bank.CreditCounter;
import fun.zsxoff.bank.CreditServiceImpl;
import fun.zsxoff.bank.CreditSorter;
import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws NegativeMoneyException {
        // Create clients.
        Client client1 = new Client("John Doe", 100);
        Client client2 = new Client("Jane Roe", 200);
        Client client3 = new Client("Zane Loe", 300);
        Client client4 = new Client("Anna Woe", 400);
        Client client5 = new Client("Anna Woe", 500);

        // Create Credit service.
        CreditServiceImpl creditService = new CreditServiceImpl();

        // Give 3 new credits to client1.
        creditService.giveCredit(client1, creditService.createCredit(100));
        creditService.giveCredit(client1, creditService.createCredit(200));
        creditService.giveCredit(client1, creditService.createCredit(300));

        // Give 2 new credits to client2.
        creditService.giveCredit(client2, creditService.createCredit(100));
        creditService.giveCredit(client2, creditService.createCredit(200));

        // Give 2 new credits to client3.
        creditService.giveCredit(client3, creditService.createCredit(900));

        // Give credits to client 4 and 5 for compare next.
        creditService.giveCredit(client4, creditService.createCredit(500));
        creditService.giveCredit(client5, creditService.createCredit(600));

        // Print credits count.
        System.out.println(CreditCounter.creditsSumm(client1));
        System.out.println(CreditCounter.creditsSumm(client2));
        System.out.println(CreditCounter.creditsSumm(client3));
        System.out.println(CreditCounter.creditsSumm(client4));
        System.out.println(CreditCounter.creditsSumm(client5));

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Sort clients.
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(client1);
        clients.add(client2);
        clients.add(client3);
        clients.add(client4);
        clients.add(client5);

        // Sort by name.
        System.out.println("---------------------------------------------------------------------");
        System.out.println(CreditSorter.sortByFullname(clients));

        // Sort by credits summ.
        System.out.println("---------------------------------------------------------------------");
        System.out.println(CreditSorter.sortByCreditsSumm(clients));

        // Sort by name and then credits summ.
        System.out.println("---------------------------------------------------------------------");
        System.out.println(CreditSorter.sortByFullnameAndCreditsSumm(clients));
    }
}
