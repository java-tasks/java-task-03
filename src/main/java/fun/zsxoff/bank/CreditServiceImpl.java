package fun.zsxoff.bank;


import fun.zsxoff.bank.Exceptions.NegativeMoneyException;

public class CreditServiceImpl implements CreditService {

    public CreditServiceImpl() {}

    @Override
    public Credit createCredit(double summa) throws NegativeMoneyException {
        return new Credit(summa);
    }

    @Override
    public void giveCredit(Client client, Credit credit) {
        client.addCredit(credit);
    }

    @Override
    public void removeCredit(Client client, Credit credit) {
        client.removeCredit(credit);
    }
}
