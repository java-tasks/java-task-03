package fun.zsxoff.bank;


import java.util.ArrayList;
import java.util.Comparator;

public class CreditSorter {
    public static ArrayList<Client> sortByFullname(ArrayList<Client> clients) {
        ArrayList<Client> sorted = new ArrayList<>(clients);

        Comparator<Client> comparator = Comparator.comparing(Client::getFullName);

        sorted.sort(comparator);

        return sorted;
    }

    public static ArrayList<Client> sortByCreditsSumm(ArrayList<Client> clients) {
        ArrayList<Client> sorted = new ArrayList<>(clients);

        Comparator<Client> comparator = Comparator.comparing(CreditCounter::creditsSumm);

        sorted.sort(comparator);

        return sorted;
    }

    public static ArrayList<Client> sortByFullnameAndCreditsSumm(ArrayList<Client> clients) {
        ArrayList<Client> sorted = new ArrayList<>(clients);

        Comparator<Client> comparator = Comparator.comparing(Client::getFullName);
        comparator.thenComparing(CreditCounter::creditsSumm);

        sorted.sort(comparator);

        return sorted;
    }
}
