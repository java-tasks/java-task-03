package fun.zsxoff.bank;


import com.google.gson.GsonBuilder;
import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class Client {
    private final UUID uuid;
    private final ArrayList<Credit> credits;
    private double cash;
    private String fullName;

    public Client() {
        this.uuid = UUID.randomUUID();
        this.credits = new ArrayList<>();
        this.cash = 0;
        this.fullName = "Unknown";
    }

    public Client(String fullName, double cash) throws NegativeMoneyException {
        this();
        this.setCash(cash);
        this.setFullName(fullName);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getCash() {
        return cash;
    }

    protected void setCash(double cash) throws NegativeMoneyException {
        if (cash < 0) {
            throw new NegativeMoneyException();
        }

        this.cash = cash;
    }

    protected ArrayList<Credit> getCredits() {
        return credits;
    }

    protected void addCredit(Credit credit) {
        this.credits.add(credit);
    }

    protected void removeCredit(Credit credit) {
        // TODO Maybe exception here.
        this.credits.remove(credit);
    }

    protected void removeCredit(UUID creditUUID) {
        // TODO Maybe exception here.
        int k = 0;
        int r = -1;

        for (Credit credit : this.credits) {
            if (credit.getUid() == creditUUID) {
                r = k;
                break;
            }

            k++;
        }

        if (r != -1) {
            this.credits.remove(r);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Double.compare(client.cash, cash) == 0 && uuid.equals(client.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, cash);
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
