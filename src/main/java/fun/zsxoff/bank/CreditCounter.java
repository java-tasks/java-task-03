package fun.zsxoff.bank;

public class CreditCounter {
    public static double creditsSumm(Client client) {
        double summ = 0;

        for (Credit credit : client.getCredits()) {
            summ += credit.getSumma();
        }

        return summ;
    }
}
